// Set up context menu at install time.
chrome.runtime.onInstalled.addListener(function() {
  // Make this contextMenu item show up on a selection or a link
  const contexts = ['selection', 'link'];
  const title = 'Search Selected Text in LH Ops Portal';
  const id = chrome.contextMenus.create({
    title: title,
    contexts,
    id: 'contextselection'
  });
});

// Set up context menu at install time.
// chrome.runtime.onInstalled.addListener(function() {
//   var context = "selection";
//   var title = "Google for Selected Text";
//   var id = chrome.contextMenus.create({"title": title, "contexts":[context],
//                                          "id": "context" + context});
// });

// The onClicked callback function.
function onClickHandler(info, tab) {
  const sText = info.selectionText;
  var url = 'https://www.google.com/search?q=' + encodeURIComponent(sText);
  window.open(url, '_blank');
}

// add click event
chrome.contextMenus.onClicked.addListener(onClickHandler);
